// CSE 002 Lab 8 Arrays
// Emma Fernandez
// April 5, 2019

// This program creates an array with a size random from 50 to 100, it then assignes random ints 
// from 0 to 99. After this, several methods are called to determine the range, the mean, and the 
// standard deviation of the array. Finnally, the array is shuffeled and printed once again.

import java.util.Random;
public class Arrays{ // required class
  public static void main (String[] args){ // main method
    Random rand = new Random();

    int sizeArray = (int) (Math.random() * 51 + 50); // random array size from, 50 to 100
    System.out.println("The size of the array is " + sizeArray);

    int []array = new int[sizeArray]; // initialize and define the array
    
    for(int i = 0; i < sizeArray; i++){ // this for loop will assign random values for each array input
      array[i] = rand.nextInt(100);
      System.out.println(array[i]); // and print all of them
    }
    
    System.out.println("The rage is: "+ getRange(array)); // call and print the range
    double mean = getMean(array); // get a mean vale to later send to another method and also print
    System.out.println("The mean is: "+ mean);
    System.out.println("The standard deviation is: "+ getStdDev(array, mean)); // call and print standard deviation
    shuffle(array); // finally shuffle array in a void method (returns nothing )
    System.out.println(Math.pow(3,2));
  }
  
  public static int getRange(int[] array){ //define range method
    int index = 0;
    int min = array[0]; // this first for loop gets the min value of the array
    for (int i=1; i<array.length; i++){
	    if (array[i] < min) { // tests if its smaller
		    min = array[i]; // define it as min
		    index = i;
	    }
   }
    
    int index2 = 0;
    int max = array[0]; // this second loop gets the max value of the array
    for (int i=1; i<array.length; i++){
	    if (array[i] > max) {
		    max = array[i];// stores it in a variable called max
		    index2 = i;
	    }
   }
    return max - min; // calculates and returns the range
    
  }
  
  public static double getMean(int[] array){ // method for getting the mean
    double sum = 0;
    for(int i = 0; i< array.length; i++){ // for loop to sum all of the variables
      sum = sum + array[i]; 
    }
    
    return sum/array.length; // calculate and return mean
  }
  
  public static double getStdDev(int[] array, double mean){ // method fro standard deviation, returns a double
    double sum = 0;
    for (int i = 0; i < array.length; i++){
      sum = sum + Math.pow((array[i] - mean), 2); // firsts it sums and squared all the valeus minus the mean
    }
    return Math.sqrt(sum/(array.length-1)); // calculates and returns the standard deviation
  }
  
  public static void shuffle(int[] array){ // final method to shuffle the array
    Random ran = new Random();
    for (int i=0; i<array.length; i++){ // this first loop exchanges the values in each array
      int randomPlace = ran.nextInt(array.length);
      int change = array[i];
      array[i] = array[randomPlace];
      array[randomPlace] = change;
	  }
    
    System.out.println("New order of array:");
    for (int j=0; j<array.length; j++){ // now this once prints them out
      System.out.println(array[j]);
	  }
	}
}// close class