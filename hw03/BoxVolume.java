// CSE 02 Homework 3: Box Volume
// Emma Fernandez
// Feb 12. 2019

// This program will ask the user for dimensions of a box and then return the value of the volume

import java.util.Scanner; // import the scanner
public class BoxVolume{ 
  // required class and method
  public static void main (String args[]){
    
    Scanner myScanner = new Scanner(System.in);// declare object and construct instance
    
    // Begin code, ask for user's inputs
    System.out.print("the width is ");
    double width = myScanner.nextDouble(); // storing the measurements in doubles
    
    System.out.print("the length is ");
    double length = myScanner.nextDouble();
    
    System.out.print("the height is ");
    double height = myScanner.nextDouble();
    
    //define output and calculate 
   
    double volume = width * length * height;
    
    System.out.println("The volume of the box is " + volume); // print out volume
    
    
  }
}