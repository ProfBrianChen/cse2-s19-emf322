// CSE 02 Homework 3: Convert
// Emma Fernandez
// Feb 12, 2019
// This program will obtain a number in meter from the user and print out its value in inches

import java.util.Scanner; // import scanner
public class Convert{
  public static void main (String args[]){
    Scanner myScanner = new Scanner(System.in);
    
    //start code: ask for a measurement in meters from the user
    System.out.print("Enter the distance in meters: ");
    double meterValue = myScanner.nextDouble(); // store number in variable
    
    //define outputs and calculate
    double inchesValue;
    inchesValue = meterValue * 39.37;
    
    //Print out the results
    System.out.printf("%.2f", meterValue);
    System.out.printf(" meters is %.4f", inchesValue);
    System.out.println(" inches");
    
  }
}