// CSE 002 Homework 8: Letters
// Emma Fernandez
// April 19, 2019

// This first program crates an array of a random size and it stores random chars in it
// the chars are leters either in caps or not
// the program then calls two different methods where the original array is sent and each char is compared to 
// separate letters from A to M and N to Z. THe methods then return an array that only has its corresponding chars
// the main method then prints the arrays and displays them all

import java.util.Random; // import the random
public class Letters{ //required class
  public static void main(String[] args){ // main method 
    Random rand = new Random();
    
    char []array = new char[rand.nextInt(20)]; // declare and allocate the array called array with a random size from 0 to 20
    String allChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"; // string with all posible chars
    System.out.println(allChars.length());

    for(int i = 0; i< array.length; i++){
      array[i] = allChars.charAt(rand.nextInt(52)); // this stores the character in a random position in the string above
    } // it chooses a random position in the string and stores it in the array
    
    System.out.print("Random character array: ");
    for (int j=0; j<array.length; j++){ // now this once prints them out
      System.out.print(array[j]); // this prints the firts array with all the chars
	  }
    System.out.println("");
    
    /////////////////  
    char[]arrayAtoM = getAtoM(array); // call method to get a new array only with chars A to M
    System.out.print("A to M character array: ");
    for (int j=0; j<arrayAtoM.length; j++){ // this prints out the array
      System.out.print(arrayAtoM[j]);
	  }
    System.out.println("");
    
    ///////////////// 
    char[]arrayNtoZ = getNtoZ(array); // call method to get array with chars N to Z
    System.out.print("N to Z character array: ");
    for (int j=0; j<arrayNtoZ.length; j++){ // now this once prints them out
      System.out.print(arrayNtoZ[j]);
	  }
    System.out.println("");
  }

  public static char[] getAtoM(char [] array){ // method to get array with chars from A to M
    int sum = 0;
    for( int w= 0; w < array.length; w++){ // this first for loop decides the size of the array that will be returned
      if (array[w] <= 'M' || array[w] <= 'm' && Character.isUpperCase(array[w]) == false){ // checks if they are from A to M
        sum += 1;
      }
    }

    char []arrayAtoM = new char[sum]; // declare the array that will be returned
    int count = 0; // this count will be used to set variables in the array
    for(int j= 0; j< array.length; j++){ // for loop to store chars from A to M
      if (array[j] <= 'M' || array[j] <= 'm' && Character.isUpperCase(array[j]) == false){
        arrayAtoM[count] = array[j];
        count++;
      }
    }
    return arrayAtoM; // returnes cropped array
  }
  
  public static char[] getNtoZ(char [] array){ // same principle as above but now from N to Z
    int sum = 0;
    for( int w= 0; w < array.length; w++){ // the only difference is changing the comparison operator
      if (array[w] >= 'N' && Character.isUpperCase(array[w])|| array[w] >= 'n' && Character.isUpperCase(array[w]) == false){
        sum += 1;
      }
    }

    char []arrayNtoZ = new char[sum]; // create array that will be returned
    int count = 0;
    for(int j= 0; j< array.length; j++){
      if (array[j] >= 'N' && Character.isUpperCase(array[j])|| array[j] >= 'n' && Character.isUpperCase(array[j]) == false){
        arrayNtoZ[count] = array[j];
        count++;
      }
    }
    return arrayNtoZ; // return for printing in the main method
  }
  
}//close class