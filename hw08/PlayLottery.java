// CSE 002 Homework 8 Lottery
// Emma Fernandez
// April 9, 2019

// This program simulates a lottery. It reads five user inputs and compares them
// to 5 randomly created integers(that cant duplicate). Then it determines if the user won
import java.util.Random; // for the random winners
import java.util.Scanner; //scanner import

public class PlayLottery{ //required class
  public static void main(String[] args){ // main method
    Scanner myScan = new Scanner(System.in); // initialize scanner
    int [] arrayUser = new int[5]; // create the user array
    System.out.println("Enter 5 numbers from 0 to 59: ");
    for(int i = 0; i < 5; i++){
      arrayUser[i] = myScan.nextInt();// stores value of user in the array
    }
    int [] arrayWinner = numbersPicked(); // call method to generate random winning numbers
    boolean didUserWin =userWins(arrayUser, arrayWinner); // call method to compare
    if(didUserWin){System.out.println("yay you win!");} // will do as expected
    else{System.out.println(":( you lose!");}
  }
  public static int[] numbersPicked(){ // first method to generate winning numbers
    Random rand = new Random(); // random
    int [] arrayWinner = new int[5]; // array winners will be returned
    System.out.print("The winning numbers are: ");
    for(int i = 0; i< 5; i++){
      arrayWinner[i] = rand.nextInt(59); // this stores the five numbers in the array
    }
    for(int i= 0; i<5; i++){ // these check if any numbers are duplicated
      for(int j=0; j<5; j++){ // if they are, it replaces one of them
        while(arrayWinner[i] == arrayWinner[j] && i != j){arrayWinner[i] = rand.nextInt(59); }
      }
      System.out.print(arrayWinner[i] + " "); // print winning numbers
    }
    System.out.println("");
    return arrayWinner; // return an array
  }
  public static boolean userWins(int [] user, int [] winner){ // method to compare
    int count =0; // to see if ALL numbers match and in the correct order
    for(int i= 0; i<5; i++){
      if(user[i] == winner[i]){count++; } //check if any are identical
    }
    if (count == 5){ return true; } // will only return true if all count is 5
    else{ return false; }
  }
}