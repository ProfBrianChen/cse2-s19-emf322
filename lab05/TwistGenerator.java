// CSE 002 lab week 6
// Emma Fernandez
// March 1, 2019


// this program will ask the user for an integer input and then print out a twist
// if the user types in an integer the twist will be printed with the number of lines
// but if the user doesnt enter an integer it will ask again

import java.util.Scanner;
// importing the scanner
// the required main method and class

public class TwistGenerator{
  public static void main (String args[]){
   
    // construct and initialize the scanner
    Scanner myScanner = new Scanner(System.in);
    
    //
    System.out.println("Enter an integer: ");
    boolean scanner1 = myScanner.hasNextInt();
   
    // infinite loop if the user fails to enter and integger
    while(scanner1 != true){
      String junkWord = myScanner.next();
      System.out.println("Error: Enter value as an integer: ");
      scanner1 = myScanner.hasNextInt();
    }
    
    // read the integer and initialize the counts
    int length = myScanner.nextInt();
    int count1 = 0;
    int leng1 = 0;
    int count2 = 0;
    int leng2 = 0;
    int count3 = 0;
    int leng3 = 0;
    
    
    // print out the twist
    while (count1 + leng1 < length){
      if(count1 % 2 == 1){
        System.out.print("/");
        leng1 += 1;
      }
      else if(count1 % 2 == 0){
        System.out.print("\\ ");
      }     
      
      count1 +=1;
    }
    System.out.println("");
    
    while (count2 + leng2 < length - 1){
      if(count2 % 2 == 0){
        System.out.print(" X ");
        leng2 += 1;
      }
      count2 += 1;
    }
    System.out.println("");

    while (count3 + leng3 < length){
      if( count3 % 2 == 1){
        System.out.print("\\");
        leng3 += 1;
      }
      else if (count3 % 2 == 0){
        System.out.print("/ ");
      }
      count3 += 1;
    }
    System.out.println("");
  }
  
}