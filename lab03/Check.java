// CSE02 Lab 3: Check
// Emma Fernandez
// Feb 8, 2019

// This program will split the check evenly after a group dinner

import java.util.Scanner; // import scanner
public class Check{
  //required main method and class
  public static void main (String[] args){
    Scanner myScanner = new Scanner( System.in ); // declare object and construct instance

// All inputs
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //original cost of the check
    double checkCost = myScanner.nextDouble(); // this will store the amount as a double in checkCost
    
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
    double tipPercent = myScanner.nextDouble(); // now it will store the tip
    tipPercent /= 100; //We want to convert the percentage into a decimal value
      
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt(); // now store the number of people: the check will be split between them
    
 // All outputs
    double totalCost;
    double costPerPerson;
    int dollars,   //whole dollar amount of cost 
      dimes, pennies; //for storing digits to the right of the decimal point 
      
      // declare variables
      totalCost = checkCost * (1 + tipPercent);
      costPerPerson = totalCost / numPeople;

    // calculate dollar amount, dime and pennies
      dollars=(int)costPerPerson;
      dimes=(int)(costPerPerson * 10) % 10;
      pennies=(int)(costPerPerson * 100) % 10;
      System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);

  }
}