// CSE 002 Lab 11
// Emma Fernandez
// May 3, 2019

// This lab does a selection sort on an array: it tests two a sorted and a reverse sorted array
import java.util.Arrays; // to print arrays later easier
public class SortArray{ // reuqired class
    public static void main(String[] args){ // main method
        int[] myArrayBest = {1, 2, 3, 4, 5, 6, 7, 8, 9}; // sorted
		int[] myArrayWorst = {9, 8, 7, 6, 5, 4, 3, 2, 1}; // reverse sort
        int timesBest = selectionSort(myArrayBest); // calls method to sort
        System.out.println("The total number of operations performed on the sorted array: " + timesBest); // num of times it compares
		int timesWorst = selectionSort(myArrayWorst); // same principle
		System.out.println("The total number of operations performed on the reverse sorted array: " + timesWorst);
    }
    public static int selectionSort(int [] list){ // this method uses selection sort to sort an array
        System.out.println(Arrays.toString(list)); // prints array
        int count =0; // this will be returned later in the method 
        for(int i = 0; i < list.length -1; i++){
            count ++; // adds one for each comparison 
            int min = list[i]; // assumes first is smallest: will later be checked
            int minIndex = i;
            for(int j = i+1; j < list.length; j++){ // this for loop will compare the position i with the rest of the positions
                count ++; // another count for each comparison
                if(list[i] > list[j]){ // if there is a smaller number
                    min = list[j]; // redefine min
                    minIndex = j; // and the index
                }
            }
            if(minIndex != i){ // if there is a smaller number it makes a switch
                int temp = list[i];
                list[i] = min;
                list[minIndex] = temp;
                System.out.println(Arrays.toString(list)); // prints the array every time there is a switch
            }
        }
        return count; // returns the number of operations
    }
}