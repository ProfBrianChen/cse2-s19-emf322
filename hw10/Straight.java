// CSE 002 Homework 10 Straight 
// Emma Fernandez
// April 30, 2019

// This code will do several things: it will create and shuffle a deck of cards as an array of ints from 0 to 51
// it will draw a five card hand as the first five ints in this shuffled array
// it will then check if this five card array has a straight poker hand, if it does it will return 1, if not it returns zero
// finally by adding the numer of times a straight was found, it calculates the percentage frequency of drawing one, in this case for one million hands

import java.util.Random; // import random to shuffle array
public class Straight{
    public static void main(String[] args){ // main method
        double sum = 0; // sums of how many straights are found
        int [] cardDeck, cardHand; // decks and hands
        int straight; // number returned wither 1 or 0
        double times = 1000000; // how many times you are playing
        for(int i = 0; i < times; i++){ // loop to play one million times
            cardDeck = cardDeckGeneration(); //this calls the first method which generates an asorted array of 52 ints
            cardHand  = drawHand(cardDeck); // this method chooses the hand as the first five members of the array
            straight = searchForStraight(cardHand); //this method calls checks the card hand for a straight (returns one if found)
            sum += straight; // rececords the number of times straight is found
        } 
        System.out.println("This program just played poker one million times! " ); // descriptions for the user
        System.out.println("It drew a straight poker hand " + (int)sum + " times" );
        System.out.print("Therefore percentage frequency of drawing a straight poker hand was: " );
        System.out.printf("%1.4f", ((sum/times)*100)); // calculates the percentage frequency
        System.out.println("%" );
    }
    public static int[] cardDeckGeneration(){ // generates the deck and shuffles it
        Random rand = new Random(); // initialize random
        int [] array = new int [52]; // create deck array
        for(int i = 0; i<52; i++){
            array [i] = i; // sorted array
        }
        for (int i=0; i<array.length; i++){ // this for loop shuffles the array
            int temp = rand.nextInt(array.length); // finds random poition
            int change = array[i]; // stores one value
            array[i] = array[temp]; // changes the two valeus
            array[temp] = change; // stored value back to new destination
        }
        return array;
    }
    public static int[] drawHand(int [] array){ // this method creates a new method with the first five ints of an array
        int[] arrayHand = new int[5]; // new smaller array
        for(int i = 0; i < 5; i++){
            arrayHand[i] = array[i] % 13; // this will make the numbers from 0 to 11 to represent actual cards
        }
        return arrayHand; // returns the new array
    }
    public static int searchForLowest(int[] array, int key){ // this method will be called on later
        int temp;
        if(key > 5 || key <= 0){ // if key is out of bounds
            return -1; // not valid
        }else{
            for(int i = 0; i< array.length-1; i++){
                for(int j = array.length -1; j> 0; j--){
                    if(array[j] < array[j-1]){
                        temp = array[j];
                        array[j] = array[j - 1];
                        array[j - 1] = temp;
                    }
                }
            }
        }
        int lowest = array[key - 1]; // returns the kth lowest value
        return lowest; // key determines which lowst (first,second,third,fourth, or fifth)
    }
    public static int searchForStraight(int[] array){ // this method actually looks for the straight
        int [] array2 = new int[5];  // creates a new array that has the five card hand in order
        for (int i=0; i<5; i++){
            array2[i] = searchForLowest(array, (i+1)); // calls the other array
        }
        if(array2[1] == (array2[0] + 1) &&  array2[2] == (array2[1] + 1) && array2[3] == (array2[2] + 1) && array2[4] == (array2[3] + 1)){
            return 1; // this if statement checks if the are all ascending, if they are it returns one
        }
        return 0; // it returns either zero if these were not ascending
    }
}