// CSE 002 Homework 10 Robot City
// Emma Fernandez 
// April 28, 2019
// This code will create a random "city" represented by a rectangle excel-like arrangement of three digit numbers that represent populations
// "Robots" land in random x,y positions and the populations turn negative
// The robots then continue to move east and jump from one population to another

import java.util.Random; // to use later for the position of robot landing
public class RobotCity{ // required class
    public static void main(String[] args){
        System.out.println("Happy City Living Peacefully!"); // first city
        int[][] cityArray = buildCity(); // calls method 
        display(cityArray); // this method is called every time you want to display city
        invade(cityArray, (int)((Math.random() * 6) + 5)); // this calls the new city with a random of robots landing
        System.out.println("Oh no! Robots have now invaded several populations!");
        display(cityArray); // thi sprints it with the newly invadin aliens
        System.out.println("Ahh! The robots are mowing east and its the end of human life!");
        for(int i = 0; i <5; i ++){ // this loop now makes robots move five positions to the right
            update(cityArray);
            display(cityArray);
            System.out.println(); // to divide the different movements
        }
    }
    public static int[][] buildCity(){ // first method builds the original city
        int [][] city = new int [(int)((Math.random() * 6) + 10)][]; // random height
        int randomWidth = (int) ((Math.random() * 6) + 10); // random width
        for(int i = 0; i< city.length; i++){
            city[i] = new int[randomWidth]; // created the width
            for(int j = 0; j < randomWidth; j++){
                city[i][j] = (int) ((Math.random() * 900) + 100); // adds the populations
            }
        }
        return city; // returns an array
    }
    public static void display(int [][] city){ // this prints the city
        for(int i = 0; i< city.length; i++){ // i is height
            for(int j = 0; j < city[0].length; j++){ // width is j
                System.out.printf("%4.0f ", (double) city[i][j]); // printf so that all spacing is correct
            }
            System.out.println(); // next row
        }
    }
    public static void invade(int[][] city, int numRobots){ // this method is the first robot landing
        Random rand = new Random();
        int one, two;
        for(int i = 0; i<= numRobots; i++){ // it chooses random positions in the city to land a determined num of robots
            one = rand.nextInt(city.length);
            two = rand.nextInt(city[0].length);
            city[one][two] = -(city[one][two]);
        }
    }
    public static void update(int [][] city){ // this method moved the robots to the east
        for(int i = city.length - 1; i >=0 ; i--){ // outer loop for height
            for(int j = city[0].length - 1; j >= 0; j--){ // inner loop foor row
                if(city [i][j] < 0 ){ // if negative
                    if(j !=city[0].length - 1){ // this if statement is needed because if not there will be a runtime error bc the loop is out of bounds
                        city[i][j] = - (city[i][j]); // makes population positive
                        if(city[i][j+1] > 0 ){ // checks if one next is positive
                            city[i][j+1] = - (city[i][j+1]); // changes the value of next population
                        }
                    }else if(j == city[0].length - 1){
                        city[i][j] = - (city[i][j]);
                    }
                }
            }
        }     
    }// close methodjavac R 
}// close class