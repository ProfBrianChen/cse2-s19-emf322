// CSE002 Lab 06
// Emma Fernandez
// March 8,2019


// PATTERN A

// This program will ask the user for an integer from 1 to 10
// it will first check if it is an integer and if it falls into the range
// if it doesnt it will ask again, and if it does it will print a triangle
// that has the input's number of rows and increases downward untill it reaches that number


// import the scanner to get the user's input
import java.util.Scanner;

// required class and main methos
public class PatternA{
  public static void main(String args[]){
    // contruct and initialize the scanner command
    Scanner myScan = new Scanner(System.in);
    
    //ask user for a number to create the triangle
    System.out.println("Enter an integer between 1 and 10");
    //  use this variable to later check if it is in rage
    int userInput = 0;
    // check if it is an integer
    boolean isAnInt = myScan.hasNextInt();
    
    // test if it is not an integer, this loop will always be activated because userInput = 0
    while(isAnInt == false || userInput > 10 || userInput <= 0){
      // loop to get an integer from the user
      while(isAnInt == false){
        String junk = myScan.next();
        System.out.println("Error: enter an integer: ");
        isAnInt = myScan.hasNextInt();
      }
      // now knowing it is an integer, store the true variable in userInput
      userInput = myScan.nextInt();
      // check if this integer falls in the range, and if not get one that does.
      while(userInput < 0 || userInput > 10){
        System.out.println("Error: enter an integer between 1 and 10");
        isAnInt = myScan.hasNextInt();
        break;
      }
    }

    // Now print the pattern A
    
    String space = " ";
    // outer loop prints the number of rows which is the userInput
    for(int numRows = 1; numRows <= userInput; numRows++){
      // inner loop actually prints the numbers in the rows
      // the numbers are increasing as you go down the rows
      for(int numbers = 1; numbers <= numRows; numbers++){
        System.out.print(numbers + space);
      }
      // to get a new row
      System.out.println(space);
    }
    
  }
}