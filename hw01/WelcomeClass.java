/// CSE 002 HW01: Welcome CLass
/// Emma Fernandez 
/// January 29, 2019

public class WelcomeClass{
  
  // Define the main method
  public static void main(String args[]){
    // Print statement with multiple lines
    System.out.println("   -----------\n " +"  | WELCOME | \n" + "   -----------"); 
    System.out.println("   ^  ^  ^  ^  ^  ^ "); 
    // In order to print \ must type '\\', only one will show up
    System.out.println("  / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println(" <-E--M--F--3--2--2->"); 
    System.out.println("  \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("   v  v  v  v  v  v");
    
  }
}