// CSE 002 Network HW 06
// Emma Fernandez

// This program will ask the user for inputs and then print out a network of squares. It will ask for:
// The width: which is the total number of characters that encompass the width of the network
// The height: same as width but in height
// The size of squares: it is the size of one side of the square (in characters)
// The size of the edge: is the length of the characters that connect the squares 



// import the scanner command
import java.util.Scanner;

public class Network{
  // required class and main method
  public static void main(String args[]){
    // construct and initiate scanner
    Scanner scanner = new Scanner(System.in);
    
    
    // STEP 1: GET Inputs
    // this program will check each input that the user types in and check if it is an integer
    // and if it is not an integer, it will ask again for another input
    // get a height input
    int height;
    // using do while loops
    do {
        System.out.print("Enter the height of the network: ");
        // checks if it is not an integer
        while (!scanner.hasNextInt()) {
            // gets rid of the input that was not an integer
            String heightNo = scanner.next();
            // asks user again for an input
            System.out.printf(" enter again: ");
        }
        // not stores the value of the integer to use later in the program
        height = scanner.nextInt();
      // end of do while loop
    } while (height < 0);
    
   
    // get a width input
    
    // the next few lines of code ask the user for the width, sice and, edge length,
    // the process is basically the same as the height... no extra and repetitive comments
    int width;
    do {
        System.out.print("Please enter a width number: ");
        while (!scanner.hasNextInt()) {
            String whdthNo = scanner.next();
            System.out.printf("   is not a valid number, enter again: ");
        }
        width = scanner.nextInt();
    } while (width < 0);
    
    
    // get a square size input
        int size;
    do {
        System.out.print("Please enter a size number: ");
        while (!scanner.hasNextInt()) {
            String sizeNo = scanner.next();
            System.out.printf("   is not a valid number, enter again: ");
        }
        size = scanner.nextInt();
    } while (size < 0);
    
   
    // get a edge input
    int edge;
    do {
        System.out.print("Please enter the edge length number: ");
        while (!scanner.hasNextInt()) {
            String edgehNo = scanner.next();
            System.out.printf("   is not a valid number, enter again: ");
        }
        edge = scanner.nextInt();
    } while (edge < 0);

    
    
    
    // PART 2
    // This part of the program will print out the network of squares when the size is ODD
    // the difference is that the edge is only one line
    // with even sized squares, the edge is two lines
    
    // define space to use in the code
    String space = " ";
     
   // check if size is an odd number
    if(size % 2 != 0 ){
      // begin the for loops to create the network of boxes
      for(int numRows = 1; numRows <= height; numRows++){
        // these will run to the length of heigh and the length of width provided by user
        for(int numColumn = 1; numColumn <= width; numColumn++){
                   
          // Build the top border line of each square (aka write code for each row)
          if(numRows % size == 0 ||numRows % size == size -1 || numRows == 1){
            // this will check to see if there is a square border to build
            if(numColumn % size == 0){
              //square boundary
              System.out.print("#");
            }
            else if (size % numColumn != 0 && size % numColumn != size -1){
              // this will check which colum to print to
              if(numColumn % size == 1){
                for(int numEdge = 1; numEdge <= edge; numEdge++){
                  // this for loop will leave spaces where further down there will be the edge connecting squares
                  System.out.print(space);
                }
              }else if (numColumn % size == 2){
                // another square boundary
                System.out.print("#-");
                
              }else {
                // this will print out the square side (horizontal)
                System.out.print("-");
              }
            }
            
            
            // now this will print the other side (vertical) of the square
          }else {
            if(numColumn % size == 0){
              // checks to see if the column we are on is part of the boundary of the square
              System.out.print(" |");
            }
            else if (size % numColumn != 0){
              if(numColumn % size == 1){
                // now this will check to see if the column is inside the square (and therefore should print spaces)
                // or outside the square and therefore print the size of the edge length to connect them
                for(int numEdge = 1; numEdge <= edge; numEdge++){
                  // for loop repeats what I described above
                  if (numRows % size == size / 2){
                    System.out.print("-");
                  }else{
                    System.out.print(space);
                  }
                }
                // this will close the other vertical side of the square
              }else if (numColumn % size == 2){
                System.out.print("|");
                
              }else {
                // this aligns the borders so that it fits the square size horizontally
                System.out.print(space);
              }
            }
           
          }
        }
        // this second part of the code is to fix some mistakes that were printed at the beginning becasue of usign the modulus operator
        if(numRows % size == size -1){
          // this if statement will add another line of code to the vertical side of the square becuase it was short of one character
          for(int numEdge = 1; numEdge <= edge; numEdge++){
            System.out.println(" ");
            System.out.print(" ");
            // this will also build the edge to connect the squares vertically
            for(int numColumn = 1; numColumn <= width + size + edge; numColumn++){
              if (numColumn % (size+ edge) == size / 2){
                // if will either be a caper or a |
                    System.out.print("|");
                  }else{
                    System.out.print(space);
            }
            }
                  
          }
          // the first row of code was missing the # in the boundary and aldo another line of code
        }else if (numRows == 1){
          System.out.println(space);
          // this adds another line of code
          for(int numColumn = 1; numColumn <= width; numColumn++){
            if(numColumn % size == 0){
              System.out.print(" |");
              // the line of code that was missing
            }
            else if (size % numColumn != 0){
              // this will connect the squares (edge)
              if(numColumn % size == 1){
                for(int numEdge = 1; numEdge <= edge; numEdge++){
                  if (numRows % size == size / 2){
                    System.out.print("-");
                  }else{
                    System.out.print(space); // or it will leave a space
                  }
                }
              }else if (numColumn % size == 2){
                System.out.print("|");
                
              }else {
                System.out.print(space);
              }
            }
          }
        }
        System.out.println(space); // this will finally make the new row each time the inner for loop ends
      }
    }
    
    
    
    // PART 3 : Even sized squares
    
    
       // check if size is an even number
    if(size % 2 == 0 ){
      // begin the for loops to create the network of boxes
      for(int numRows = 1; numRows <= height; numRows++){
        // these will run to the length of heigh and the length of width provided by user
        for(int numColumn = 1; numColumn <= width; numColumn++){
 
          if(numRows % size == 0 || numRows % size == size - 1){
            if(numColumn % size == 0){
              System.out.println("#");
            }else{
              // this will check which colum to print to
              if(numColumn % size == 1){
                for(int numEdge = 1; numEdge <= edge; numEdge++){
                  // this for loop will leave spaces where further down there will be the edge connecting squares
                  System.out.print(space);
                }
              }else if (numColumn % size == 2){
                // another square boundary
                System.out.print("#");
                
              }else {
                // this will print out the square side (horizontal)
                System.out.print("-");
              }
            }
            
            
            // now this will print the other side (vertical) of the square
          }else {
            if(numColumn % size == 0){
              // checks to see if the column we are on is part of the boundary of the square
              System.out.print(" |");
            }
            else if (size % numColumn != 0){
              if(numColumn % size == 1){
                // now this will check to see if the column is inside the square (and therefore should print spaces)
                // or outside the square and therefore print the size of the edge length to connect them
                for(int numEdge = 1; numEdge <= edge; numEdge++){
                  // for loop repeats what I described above
                  if (numRows % size == size / 2){
                    System.out.print("-");
                  }else{
                    System.out.print(space);
                  }
                }
                // this will close the other vertical side of the square
              }else if (numColumn % size == 2){
                System.out.print("|");
                
              }else {
                // this aligns the borders so that it fits the square size horizontally
                System.out.print(space);
              }
            }
           
          }
        }
        // this second part of the code is to fix some mistakes that were printed at the beginning becasue of usign the modulus operator
        if(numRows % size == size -1){
          // this if statement will add another line of code to the vertical side of the square becuase it was short of one character
          for(int numEdge = 1; numEdge <= edge; numEdge++){
            System.out.println(" ");
            System.out.print(" ");
            // this will also build the edge to connect the squares vertically
            for(int numColumn = 1; numColumn <= width + size + edge; numColumn++){
              if (numColumn % (size+ edge) == size / 2){
                // if will either be a caper or a |
                    System.out.print("|");
                  }else{
                    System.out.print(space);
            }
            }
                  
          }
          // the first row of code was missing the # in the boundary and aldo another line of code
        }else if (numRows == 1){
          System.out.println(space);
          // this adds another line of code
          for(int numColumn = 1; numColumn <= width; numColumn++){
            if(numColumn % size == 0){
              System.out.print(" |");
              // the line of code that was missing
            }
            else if (size % numColumn != 0){
              // this will connect the squares (edge)
              if(numColumn % size == 1){
                for(int numEdge = 1; numEdge <= edge; numEdge++){
                  if (numRows % size == size / 2){
                    System.out.print("-");
                  }else{
                    System.out.print(space); // or it will leave a space
                  }
                }
              }else if (numColumn % size == 2){
                System.out.print("|");
                
              }else {
                System.out.print(space);
              }
            }
          }
        }
        System.out.println(space); // this will finally make the new row each time the inner for loop ends
      }
    }
    
  }
}