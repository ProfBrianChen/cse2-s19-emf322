// CSE002 Lab07
// Emma Fernandez
//March 22, 2019

// This program will create a gramatically correct random sentence

import java.util.Random;
import java.util.Scanner;

public class Methods{
  
  // Method to generate an adjective as a String 
  public static String adjective(int ranNum1){
    String adjWord1 = "happy ";
    switch(ranNum1){
      case 1:
        adjWord1 = "pretty ";
        break;
      case 2:
        adjWord1 = "scary ";
        break;
      case 3:
        adjWord1 = "angry ";
        break;
      case 4:
        adjWord1 = "brown ";
        break;
      case 5:
        adjWord1 = "white ";
        break;
      case 6:
        adjWord1 = "crazy ";
        break;
      case 7:
        adjWord1 = "energetic ";
        break;
      case 8:
        adjWord1 = "evil ";
        break;
      case 9:
        adjWord1 = "smart ";
        break;
    }
    return adjWord1;
    
  }
  
    // Method to generate a noun for the subject as a String 
  public static String nounSubject(int ranNum1){
    String nounWord1 = "monster ";
    switch(ranNum1){
      case 1:
        nounWord1 = "dog ";
        break;
      case 2:
        nounWord1 = "cat ";
        break;
      case 3:
        nounWord1 = "robot ";
        break;
      case 4:
        nounWord1 = "elephant ";
        break;
      case 5:
        nounWord1 = "alien ";
        break;
      case 6:
        nounWord1 = "plane ";
        break;
      case 7:
        nounWord1 = "camaleon ";
        break;
      case 8:
        nounWord1 = "turtle ";
        break;
      case 9:
        nounWord1 = "bat ";
        break;
    }
    return nounWord1;
    
  }
  
// Method to generate a verb in past tense as a String 
  public static String verbPast(int ranNum1){
    String verbWord1 = "ran by ";
    switch(ranNum1){
      case 1:
        verbWord1 = "ate ";
        break;
      case 2:
        verbWord1 = "laughed with ";
        break;
      case 3:
        verbWord1 = "passed ";
        break;
      case 4:
        verbWord1 = "danced with ";
        break;
      case 5:
        verbWord1 = "ate with ";
        break;
      case 6:
        verbWord1 = "flew by ";
        break;
      case 7:
        verbWord1 = "met with ";
        break;
      case 8:
        verbWord1 = "looked at ";
        break;
      case 9:
        verbWord1 = "angered ";
        break;
    }
    return verbWord1;
    
  }
  
  // Method to generate a noun for the object of the sentence 
  public static String nounObject(int ranNum1){
    String nounWord1 = "tiger ";
    switch(ranNum1){
      case 1:
        nounWord1 = "cow ";
        break;
      case 2:
        nounWord1 = "owl ";
        break;
      case 3:
        nounWord1 = "deer ";
        break;
      case 4:
        nounWord1 = "lion ";
        break;
      case 5:
        nounWord1 = "horse ";
        break;
      case 6:
        nounWord1 = "bear ";
        break;
      case 7:
        nounWord1 = "mosquito ";
        break;
      case 8:
        nounWord1 = "butterfly ";
        break;
      case 9:
        nounWord1 = "frog ";
        break;
    }
    return nounWord1;
    
    
// This is the main method of this class, it will be run and call all other methods    
  }
  public static void main (String args []){
    Scanner myScan = new Scanner(System.in);
    Random randomGenerator = new Random();
    
    String userInput = "yes";
    // define the subject of the sentence to use later outside the loop
    String nounSentenceSubject = "define";
    while (userInput.equals("yes")){
      
      // generate adjectives
      // First adjective
      int randomNumber1 = randomGenerator.nextInt(10);
      String adjSentence1 = adjective(randomNumber1);
      // Second adjective
      int randomNumber2 = randomGenerator.nextInt(10);
      String adjSentence2 = adjective(randomNumber2);
       // Third adjective as string
      int randomNumber3 = randomGenerator.nextInt(10);
      String adjSentence3 = adjective(randomNumber3);     

      
      //generate the subject noun 
      int randomNumber4 = randomGenerator.nextInt(10);
      nounSentenceSubject = nounSubject(randomNumber4);

      //generate the verb in past tense
      int randomNumber5 = randomGenerator.nextInt(10);
      String verbSentence = verbPast(randomNumber4);
      
      //generate the object noun
      int randomNumber6 = randomGenerator.nextInt(10);
      String nounSentenceObject = nounObject(randomNumber4);
      
      System.out.println("The " + adjSentence1 + adjSentence2 + nounSentenceSubject + verbSentence + "the " + adjSentence3 + nounSentenceObject);
      System.out.println("Would you like another random sentence?");
      System.out.println("Type yes for another random sentence or type anything else for the rest of the story");
      userInput = myScan.next();
    }
    
    // Phase 2 of the lab will happen when the user types in something other than "yes"
    // the subject will be the same
    // now a paragraph will be printed

    // This new method that I will call, will carry the same Subject noun and create the paragraph
    String paragraphSentece1 = paragraphSen1Method(nounSentenceSubject);
    String paragraphSentece2 = paragraphSen2Method(nounSentenceSubject);  
    String paragraphSentece3 = paragraphSen3Method(nounSentenceSubject); 
    // now this n
    System.out.println( paragraphSentece1 );
    System.out.println( paragraphSentece2 );
    System.out.println( paragraphSentece3 );
    
    // conclusion sentence
    
    
  }
  
  // method for printing the first sentence of the paragraph
    public static String paragraphSen1Method(String nounSub){
      Random randomGenerator = new Random();
      String parag = "particulary ";
      
      // Adjective 1
      int randomNumber7 = randomGenerator.nextInt(10);
      String adjSentence4 = adjective(randomNumber7);
      // Adjective 2
      int randomNumber8 = randomGenerator.nextInt(10);
      String adjSentence5 = adverbPhaseTwo(randomNumber8);
      // Adjective 3
      int randomNumber9 = randomGenerator.nextInt(10);
      String adjSentence6 = adjective(randomNumber9);      

      //generate the verb in past tense
      int randomNumber10 = randomGenerator.nextInt(10);
      String adjSentence2 = adjecPhaseTwo(randomNumber10);
      
      //generate the object noun
      int randomNumber11 = randomGenerator.nextInt(10);
      String nounSentenceObject2 = nounObject(randomNumber11);
      
      // this creates the first action sentence
      parag = "This " + nounSub + "was " + adjSentence5 + adjSentence2 + "to the " + adjSentence6 + nounSentenceObject2;
      
    return parag;
    
  }
  
    // method for printing the second sentence of the paragraph
    public static String paragraphSen2Method(String nounSub){
      Random randomGenerator = new Random();
      String parag = "particulary ";
      nounSub = "It ";
      
      // Adjective 1
      int randomNumber7 = randomGenerator.nextInt(10);
      String verbPast2 = verbPast(randomNumber7);
      // Adjective 2
      int randomNumber8 = randomGenerator.nextInt(10);
      String adjSentence5 = adverbPhaseTwo(randomNumber8);
      // Adjective 3
      int randomNumber9 = randomGenerator.nextInt(10);
      String concreteNoun1 = concreteNoun(randomNumber9);      

      //generate the verb in past tense
      int randomNumber10 = randomGenerator.nextInt(10);
      String adjSentence2 = adjecPhaseTwo(randomNumber10);
      
      //generate the object noun
      int randomNumber11 = randomGenerator.nextInt(10);
      String nounSentenceObject2 = nounObject(randomNumber11);
      
      // this creates the first action sentence
      parag = nounSub + "used " + adjSentence5 + adjSentence2 + concreteNoun1 + "and then " + verbPast2 + "the " + nounSentenceObject2;
      
    return parag;
    
  }
  
    // method for printing the first sentence of the paragraph
    public static String paragraphSen3Method(String nounSub){
      Random randomGenerator = new Random();
      String parag = "particulary ";
      
      // Adjective 1
      int randomNumber7 = randomGenerator.nextInt(10);
      String verbPast2 = verbPast(randomNumber7);
      
      //generate the object noun
      int randomNumber11 = randomGenerator.nextInt(10);
      String nounSentenceObject2 = nounObject(randomNumber11);
      
      // this creates the first action sentence
      parag = "That " + nounSub + verbPast2 + "the " + nounSentenceObject2 + "!";
      
    return parag;
    
  }
  
    // method for Phase two
    public static String adverbPhaseTwo(int ranNum1){
    String adverbWord1 = "particulary ";
    switch(ranNum1){
      case 1:
        adverbWord1 = "rarely ";
        break;
      case 2:
        adverbWord1 = "pecularly ";
        break;
      case 3:
        adverbWord1 = "happily ";
        break;
      case 4:
        adverbWord1 = "sadly ";
        break;
      case 5:
        adverbWord1 = "angrily ";
        break;
      case 6:
        adverbWord1 = "nervously ";
        break;
      case 7:
        adverbWord1 = "thoughtfuly ";
        break;
      case 8:
        adverbWord1 = "scarcely ";
        break;
      case 9:
        adverbWord1 = "slowly ";
        break;
    }
    return adverbWord1;
    
  }
  
    
    // method for Phase two
    public static String adjecPhaseTwo(int ranNum1){
    String adverbWord1 = "mean ";
    switch(ranNum1){
      case 1:
        adverbWord1 = "nice ";
        break;
      case 2:
        adverbWord1 = "agressive ";
        break;
      case 3:
        adverbWord1 = "kind ";
        break;
      case 4:
        adverbWord1 = "rude ";
        break;
      case 5:
        adverbWord1 = "angrily ";
        break;
      case 6:
        adverbWord1 = "lovable ";
        break;
      case 7:
        adverbWord1 = "bad ";
        break;
      case 8:
        adverbWord1 = "thoughtful ";
        break;
      case 9:
        adverbWord1 = "good ";
        break;
    }
    return adverbWord1;
    
  }
  
      // method for the paragraph: to create another type of noun: concrete
    public static String concreteNoun(int ranNum1){
    String nounWord = "words ";
    switch(ranNum1){
      case 1:
        nounWord = "phrases ";
        break;
      case 2:
        nounWord = "advice ";
        break;
      case 3:
        nounWord = "recommendations ";
        break;
      case 4:
        nounWord = "langage ";
        break;
      case 5:
        nounWord = "apologies ";
        break;
      case 6:
        nounWord = "comments ";
        break;
      case 7:
        nounWord = "stares ";
        break;
      case 8:
        nounWord = "jokes ";
        break;
      case 9:
        nounWord = "inside jokes ";
        break;
    }
    return nounWord;
    
  }
  
}