// CSE 002 Lab 10: Two dimensional Arrays
// Emma Fernadez
// April 19, 2019

// this code creates three matrices as two dimensional arrays, they vary height, width and whether they are
// row or column major. The program then tris to add the frist two and the frist and third
import java.util.Random; // to create the random width and height of matrices
public class lab10{ // required
    public static void main(String[] args){
        Random rand = new Random(); // initialize
        int width1 = (int) (Math.random() * 4) + 1; // first random width
        int height1 = (int) (Math.random() * 4) + 1; /// first random height
        boolean format1 = true; // this is for row major format
        boolean format2 = false;// column major format 
        int width2 = (int) (Math.random() * 4) + 1; // second w
        int height2 = (int) (Math.random() * 4) + 1; // second h
        int [][] arrayA = increasingMatrix (width1, height1, format1); // this calls the methos that creates the first array
        System.out.print("Matrix A :");
        printMatrix(arrayA, format1); // prints the first row major array
        System.out.print("Matrix B :");
        int [][] arrayB = increasingMatrix (width1, height1, format2); // method for second array with column major
        printMatrix(arrayB, format2);
        System.out.print("Matrix C :");
        int [][] arrayC = increasingMatrix (width2, height2, format1); // third array, diff w and h
        printMatrix(arrayC, format1);
        System.out.println("Adding matrices A and B after translating B from column to row major: ");
       int [][] arrayAB = addMatrix(arrayA, format1, arrayB, format2); //Now add Matrix A and B
       printMatrix (arrayAB, format1);
        System.out.println("Adding matrices A and C: "); //Now add Matrix A and C
        int [][] arrayAC = addMatrix(arrayA, format1, arrayC, format1);
        printMatrix (arrayAC, format1); // prints if matrices have same dimensions

    }

    public static int [] [] increasingMatrix(int width, int height, boolean format){ // this method created a matrix
        int [][] array = new int[height] []; // returns a two demensional array
        int sum = 0; // to later make members
        if(format){ // row-major is when format == true 
            for(int i = 0; i< height; i++){
                array[i] = new int[width]; // creates space
                for(int j = 0; j < width; j++){
                    sum = sum + 1; // increasing matrix
                    array[i][j] = sum;
                }
            }
        } else{
            for(int i = 0; i< height; i++){ // if it is column major
                array[i] = new int[width]; // makes space
            }
            for(int i = 0; i< width; i++){ // this loop creates a column major matrix
                for(int j = 0; j < height; j++){
                    sum = sum + 1;
                    array[j][i] = sum; // increasing downward
                }
            }
        }
        return array; // returns a two dimensional array
    }
    public static void printMatrix(int [][] array, boolean format){ // prints any matrix
        if (array == null ){ // if the array is empty!!!
            System.out.println("The array was empty! Can't print it! ");
            return;
        }
        if(format){ // if true it says its row major
            System.out.println("height " + array.length + " and width " + array[0].length +" in row major format");      
        }else{ // if flase it says its a column major
            System.out.println("height " + array.length + " and width " + array[0].length +" in column major format");
        }
        for(int i = 0; i< array.length; i++){ // continues to print the array
            for(int j = 0; j < array[i].length; j++){
                System.out.print(array[i][j] + " "); // prints each position
            }
            System.out.println(); // makes a new row
        }
    }
    public static int[][] translate(int [][] array2){ // this translates a matrix from row major to column major
        int [][] array = new int [array2.length][]; // makes a new array to return later
        int sum = 0;
        for(int i = 0; i< array2.length; i++){
            array[i] = new int[array2[0].length]; // makes space
        }
        for(int i = 0; i< array2.length; i++){
            for(int j = 0; j < array2[0].length; j++){
                sum = sum + 1;
                array[i][j] = sum; // makes a row major format
            }
        }
        return array;
    } // the following method tries to add two matrices, it will only work if they are the same w and h
    public static int[][]addMatrix(int [] [] array1, boolean format1, int [][] array2, boolean format2){
        if(array1.length != array2.length || array1[0].length != array2[0].length ){
            System.out.println("Unable to add input matrices"); // if diff dimensions
            return null; // returns nothing
        }
        if(format2 == false){
            array2 = translate(array2); // if it is a column major format it translates it to later add
        }
        int [][] array = new int [array2.length][]; // makes space
        for(int i = 0; i< array2.length; i++){
            array[i] = new int[array1[0].length]; /// makes space
            for(int j = 0; j < array1[0].length; j++){
                 array[i][j] = array1[i][j] + array2[i][j]; // adds the matrices by adding seach position
            }
        }
        return array; // returns the added matrix 
    } 
} 