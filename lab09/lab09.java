// CSE lab09 Searching
// Emma Fernandez
// April 12 2019


// This lab uses either binary search or linear search to find a integer in an array's position
// it asks the user which test it want to run, the size of the array, and the input to look for
// it then generates an array with random integers and uses the specified test to look for a value

import java.util.Random; // to create an array of random ints
import java.util.Scanner; // scanner class import
import java.util.Arrays; // to arrange array in order

public class lab09{ // required class
    public static void main(String[] args){ // main method
        Scanner myScan = new Scanner(System.in); // import and build scanner class
        System.out.println("Do you want to do a linear or a binary search? "); // ask user to type in the search they want 
        String userInput = myScan.next(); // store input
        boolean userValidIn, isSize, isSearch; // all of these are variables that will be used later on in the code
        int size, search;
        do{ // do while loop to make sure the correct input was typed
            if(userInput.equals("linear")){ // if user typed linear
                userValidIn = true; // this exits the do loop that checks if the user's first input is either 'linear' or 'binary'
                System.out.println("What do you want the array size to be? "); // size of random array
                isSize = myScan.hasNextInt();
                while(isSize == false){ // this will run if the user typed something other than an int
                    String wrong = myScan.next(); // erase bad input
                    System.out.println("wrong input, enter an int: "); // ask again
                    isSize = myScan.hasNextInt();
                }
                size = myScan.nextInt(); // now when it is an integer, it is stored
                System.out.println("What integer do you want to search for? "); // ask for int to search
                isSearch = myScan.hasNextInt();
                while(isSearch == false){ // smae test, to check if input is an int
                    String wrong = myScan.next();
                    System.out.println("wrong input, enter an int: ");
                    isSearch = myScan.hasNextInt(); // infinitely asks until user types int
                }
                search = myScan.nextInt(); // stored int from user
                int []array = createArray(size); // array of size 'size' is created from calliing a method
                int indexArrayMember = searchForIndex(array, search); // now call method to carry out linear search, it returns the position where its found
                System.out.print("Array: "); // prints random array of size 'size'
                    for(int i = 0; i < array.length; i++){
                        System.out.print(array[i] + " ");
                    }
                System.out.println();
                System.out.println("integer is in position " + indexArrayMember); // prints the position of the integer
            }else if(userInput.equals("binary")){  // now this will run if the user types in binary
                userValidIn = true;
                System.out.println("What do you want the array size to be? "); // it follows the same method as linear, just calls different methods
                isSize = myScan.hasNextInt();
                while(isSize == false){
                    String wrong = myScan.next();
                    System.out.println("wrong input, enter an int: ");
                    isSize = myScan.hasNextInt();
                }
                size = myScan.nextInt();
                System.out.println("What integer do you want to search for? "); // again, same process as before, just a different if statement
                isSearch = myScan.hasNextInt();
                while(isSearch == false){
                    String wrong = myScan.next();
                    System.out.println("wrong input, enter an int: ");
                    isSearch = myScan.hasNextInt();
                }
                search = myScan.nextInt();
                int [] arrayBinary = createAscendingArray(size); // now this will call another method that creates an array of that size, and arranges the integers in order
                int index = indexSearchBinary(arrayBinary, search);// with that array created, it is sent to another method which binary searches fo the number
                System.out.print("Array: "); // print out array that was given back in ascending order
                for(int i = 0; i < arrayBinary.length; i++){
                    System.out.print(arrayBinary[i] + " ");
                }
                System.out.println();
                System.out.println("integer is in position " + index); // prints out where in the array the input was found

            }else{
                System.out.println("Not valid, type 'linear' or 'binary': "); // if input is neither insert nor shorten
                userInput = myScan.next(); // read once again the input from user and test if it is either shorten or insert
                userValidIn = false; // this will keep it looping
            }
        }while(userValidIn == false);
    }

    public static int searchForIndex(int []array, int number){ // this method searches for an index linearly
        int index = 0;
        for(int i = 0; i < array.length; i++){ // loop do go through every member of the array
            if (array[i] == number){
                index = i; // breaks loop once it is found
                break;
            }else{ index = -1;} // returns -1 if it is never found
        }
        return index; // returns the index where it was found (if it is ever found)
    }

    public static int []createArray(int size){ // this creates an array of a given size with random integers (returns the array)
        //Random rand = new Random(); // to get the random ints
        int []array = new int[size];
        for(int i = 0; i < array.length; i++){
            array[i]= (int)(Math.random() * (size+1)); // creates and assigns the inputs
        }
        return array; // returns the array
    }

    public static int indexSearchBinary(int []array, int index){ // this method searches for an input in a binary search
        int first = 0;
        int last = array.length;
        int mid = last /2; // mid is the middle poing in the array
        boolean found = false;

        if(index > array.length){ // this happens when the index the user wants to search is bigger than the array size
            return -1;
        }
        while(first <= last){ // loop to run binary search
            if(array[mid] == index){
                found = true; // when found it breaks the loop and it returns the mid
                break;
            }else if(array[mid]< index){first = mid + 1;} // new boundaries when the index is higher
            else{last = mid - 1;} // new boundaries when the index is lower
            mid = (first + last)/2; // re-calculates the mean
        }
        if(found){
            return mid; // if found it return the position
        }else{
            return -1;
        }
    }

    public static int []createAscendingArray(int size){ // this method generates an array is ascending order
        Random rand = new Random(); // to assign random integers
        int [] array = new int[size]; // create array that will be the output
        for(int i = 0; i< array.length; i++){
            array[i] = rand.nextInt(array.length); // creates random integers to store in array
        }
        Arrays.sort(array); // command that, after importing Arrays, sorts them
        return array; // returns ascending array     
    }
}