// CSE02 Homework 2: Arithmetic
// Emma Fernandez
// Feb 5, 2019
// This program will calculate the cost of items bought in a store plus tax

public class Arithmetic{
  // required method and class
  
  public static void main(String args[]){
    
    //input variables
    int numPants = 3; // pairs of pants
    double pantsPrice = 34.98; // cost of pairs of pants
    int numShirts = 2; // number of shirt a
    double shirtPrice = 24.99; // shirt cost
    int numBelts = 1; // numb belts
    double beltCost = 33.99; // belt cost
    double paSalesTax = 0.06; // tax rate
    
    // declare variables
    double totalCostOfPants;   //total cost of pants
    double totalCostOfShirts; 
    double totalCostOfBelts;
    double costBeforeTax;

    // calculations
    // total costs
    totalCostOfPants = numPants * pantsPrice;
    totalCostOfShirts = numShirts * shirtPrice;
    totalCostOfBelts = numBelts * beltCost;
    // sale tax per item
    int saleTaxPants = (int) (totalCostOfPants * paSalesTax * 100);
    int saleTaxShirts = (int) (totalCostOfShirts * paSalesTax * 100);
    int saleTaxBelts = (int) (totalCostOfBelts * paSalesTax * 100);
    
    //total cost of purchase before tax
    costBeforeTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
    int totalSaleTax = (int) ((saleTaxPants + saleTaxShirts + saleTaxBelts));
    int costAfterTax = (int) ((costBeforeTax + (totalSaleTax/100) )* 100);
   
    //Print command
    System.out.println("Cost of pants was $" + totalCostOfPants + " and the sales tax paid was $" + (saleTaxPants/100.0));
    System.out.println("Cost of shirts was $" + totalCostOfShirts + " and the sales tax paid was $" + (saleTaxShirts/100.0));
    System.out.println("Cost of belts was $" + totalCostOfBelts + " and the sales tax paid was $" + (saleTaxBelts/100.0));   
    System.out.println("Total cost of purchase before tax was $" + costBeforeTax);
    System.out.println("Total sales tax was $" + (totalSaleTax/ 100.0));
    System.out.println("Total cost of purchase including sales tax was $" + (costAfterTax/100.0) );
    
    //Total cost of each kind of item (i.e. total cost of pants, etc)
//Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
//Total cost of purchases (before tax)
//Total sales tax
//Total paid for this transaction, including sales tax. 

    
    //Next, compute and display the cost of each of item type 
    //and the total sales tax paid for buying all of that item. 
    //Finally, compute and display the total cost of the purchases (before tax), 
    //the total sales tax, and the total cost of the purchases (including sales tax). 

//When you display any price, make sure to eliminate 
    //any fractional pennies that might get printed as extra decimals after the first two decimal values

  }
  
}