// CSE 02 Lab02: Cyclometer 
// Feb 1, 2019
// Emma Fernandez
// This program is a bicycle cyclometer, measures speed and distance
/// it will print out : minutes, counts, distance for each trip, and distance for the two trips combined

public class Cyclometer{
  // required main method
  
  public static void main (String args[]){
    // variables
    int secsTrip1=480;  // seconds in first trip
    int secsTrip2=3220;  // seconds in second trip
		int countsTrip1=1561;  // number of rotations in first wheel first trip
		int countsTrip2=9037; // number of rotations in first wheel second trip
    
    // intermediate variables and output data
    double wheelDiameter=27.0,  // diameter first wheel 
  	PI=3.14159, // pi
  	feetPerMile=5280,  // conversion ft per mile
  	inchesPerFoot=12,   // conversion in per ft
  	secondsPerMinute=60;  // conversion seconds to min
	  double distanceTrip1, distanceTrip2,totalDistance;  // define distance as double variable
    
    // print commands
    System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	  System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");

    	// Calculations for output data
	  distanceTrip1=countsTrip1*wheelDiameter*PI; // Distance in inches and multiplied by pi, gives count
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	  totalDistance=distanceTrip1+distanceTrip2;
    
    
    	//Print command for the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");

  } //closes method
  
} // closes class