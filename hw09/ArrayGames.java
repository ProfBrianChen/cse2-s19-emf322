// CSE 002 Homework 9 Array Games
// Emma Fernandez
// April 16, 2019

// This homework works with arrays, it asks the user if they want to shorten or insert an array
// it then generates and prints wither one or two arrays of a random size between 10 and 20 with random ints
// it checks the user's input and does what is asks for

import java.util.Scanner; // to read user input
import java.util.Random; // to get random numbers
public class ArrayGames{ //required class and main method
    public static void main (String args[]){
        Scanner myScan = new Scanner(System.in); // import and build scanner class
        boolean stringIs = false;
  
        System.out.println("Would you like to run insert or shorten?");// print asking user for input
        System.out.println("Type 'insert' or 'shorten'");
        String userInput = myScan.next(); // ask and store user input
        do{ // makes sure the correct input is given
            if(userInput.equals("insert")){ // if user wants to insert
                stringIs = true; // this will stop the do loop 
                int [] arrayInput1 = generate((int) (Math.random() * 10 + 10)); // calls method to generate arrays
                print(arrayInput1, "Input 1: "); // calls method to print arrays
                int [] arrayInput2 = generate((int) (Math.random() * 10 + 10));
                print(arrayInput2, "Input 2: ");
                int []arrayOutput = insert(arrayInput1, arrayInput2); // calls method that inserts one array into another
                print(arrayOutput, "Output: ");// prints final output
            }else if(userInput.equals("shorten")){
                stringIs = true;
                System.out.println("Type in the member you want to delete: "); // if user wnats to delete, ask where
                int userInput2 = myScan.nextInt();
                int [] arrayInput1 = generate((int) (Math.random() * 10 + 10)); // generate array
                print(arrayInput1, "Input 1: "); // only one array in this case
                int []arrayOut = shorten(arrayInput1, userInput2); // call method to shorten array and print
                print(arrayOut, "Output: ");
            }else{
                System.out.println("Not valid, type 'shorten' or 'insert': "); // if input is neither insert nor shorten
                userInput = myScan.next(); // read once again the input from user and test if it is either shorten or insert
                stringIs = false; // this will keep looping
            }
        }while(stringIs == false);
    }  
    
    public static int [] generate (int size){ // method that generates the array of a determined size 
        Random rand = new Random();
        int []arrayInput = new int[size]; // create and build array
        for(int i = 0; i < arrayInput.length; i++){
            arrayInput[i]= (int) rand.nextInt(arrayInput.length); // assign a random number
        }
        return arrayInput; //return that array that will be an input
    }

    public static void print(int [] array, String text){ // method that prints array
        System.out.print(text); // prints which array it is
        for(int i = 0; i < array.length; i++){
            System.out.print(array[i] + " "); // printing members
        }
        System.out.println();
    } // returns nothing 

    public static int []insert (int []array1, int [] array2){ // first method that inserts one array into another
        Random rand = new Random();
        int size = array1.length + array2.length; // to use later in code
        int place = rand.nextInt(array1.length); // random member where it will be inserted

        int []array = new int[size]; // create array

        for(int i = 0; i < place; i++){ // stops at the random member place
            array[i]= array1[i];
        }
        for(int i = place; i < place + array2.length; i++){ // starts placing the other arrayy's values in the first array position
            array[i]= array2[i - place];
        }
        for(int i = place + array2.length; i < size; i++){ // finishes the placing of members
            array[i]= array1[i - array2.length];
        }
        return array; // returns new array of large length
    }

    public static int [] shorten(int [] array, int member){ // next mehthod that will delete one member of array
        if (member > array.length || member < 0){
            return array; // returns same aaray if it is out of range
        }else{
            int []arrayOut = new int[array.length - 1]; // creates array with one member smalles
            for(int i = 0; i < member; i++){
                arrayOut[i]= array[i];
            }
            for(int i = member; i < arrayOut.length; i++){ // exchanges and deletes one member of array
                arrayOut[i]= array[i + 1];
            }
            System.out.println("Input 2: " + member); // prints where the member deleterd is found
            return arrayOut;
        }
    }

}