// CSE 002 HomeWork 7 String Analysis
// Emma Fernandez
// March 26, 2019

// This program will analyze a string input from the user and determine if all the characters are letters
// it will check each character one by one and determine if there is a digit
// the user will also choose if they want all the string analyzed or just a certain number of characters


import java.util.Scanner; // import the scanner commans

public class StringAnalysis{ // required class
  public static void main (String[] args){ // main method required: will run first
    Scanner myScan = new Scanner(System.in); // initialize and build scanner

    System.out.println("Enter a string: "); // ask user for input and store it as string
    String stringInput = myScan.next();

    System.out.println("Do you want to check all the characters or just a certain number? Type all or type the number");
    if (myScan.hasNextInt() == true){ // if the user types in a whole number, the program will only chech that number of characters
      int numCharacters = myScan.nextInt(); // this stores the number of characters
      boolean areCharLetters = areCharLettersMethod(stringInput, numCharacters); // this will call the method
      
      if(areCharLetters){ // the method returns a boolean that if true, all characters checked are letters
        System.out.println("yay! the string characters are all letters!");
      }
      else // if false, there is at least one digit
      System.out.println("aw :( the string characters are not all letters");
      
    } else{ // if the user typed in anything other than a whole number, this will run
      boolean areCharLetters = areCharLettersMethod(stringInput); // calls the method ( same name)
      if(areCharLetters){ // same concept, method returns a boolean
        System.out.println("yay! the string characters are all letters!");
      }
      else
      System.out.println("aw :( the string characters are not all letters");      
    }
  }
  
  // method if user only wants to check a certain number of characters
  public static boolean areCharLettersMethod(String stringIn, int b){
    boolean letters = true; // this will be returned
    int j = 0; // count to stop the loop later
    for(int c = 0; c < b; c++){
      if(Character.isDigit(stringIn.charAt(c))){ // this command checks if the character in position c is a digit
        letters = false; // if it is a digit, now the method returns false
        j++; 
      }else{
        j++; // j will count every character
      }
      if (j == stringIn.length()){ // if all the characters are checked
        break; // this stops the loop
      }
    }
    return letters; // return a boolean
  } 
  
    public static boolean areCharLettersMethod(String stringIn){ // second method if all characters are checked
    boolean letters = true;
    for(int c = 0; c < stringIn.length(); c++){ // loop will run for the number of characters in the string
      if(Character.isDigit(stringIn.charAt(c))){
        letters = false;  // if there is a digit in character number "c", letters will be false
      }
    }
    return letters;
  }
}