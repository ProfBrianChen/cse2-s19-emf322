// CSE002 Homework 7: Area of three different shapes
// Emma Fernandez
// March 26,2019

// This program will calculate the area of a shape
// The user will choose which shape: circle, triangle or rectangle and also provide the dimensions
// the program will keep asking the user for inputs until it gets a valid one
// the dimensions will then be sent to a method specific for that shape to calculate the area
// the area will be returned to the main method and printed there

// import scanner command
import java.util.Scanner;

//required class
public class Area{
  // required main method: this will run first! ... void type: means returns nothing
  public static void main (String[] args){
    Scanner myScan = new Scanner(System.in); // construct and initialize scanner commans
    
    String shape = "tbd"; // define and initialize the string to be later changed
    boolean isShape = false; //boolean will become true when the user types in true shape
    
    System.out.println("Choose a shape (circle, rectangle or triangle): ");
    shape = myScan.next(); // read the user's input as a string with next();
    
    do{ // do loop will always run at least once : in this case until isShape becomes true
      if(shape.equals("circle")){ // stringName.equals is used to compare two strings
        isShape = true;
        break; // this will break out of the if statement and the loop will not run again because shape is true
      }else if(shape.equals("rectangle")){
        isShape = true;
        break;
      }else if(shape.equals("triangle")){
        isShape = true;
        break;
      }else {
        System.out.println("Not a valid shape, enter again: ");
        shape = myScan.next(); // read once again the input from user and test if it is a valid shape
        isShape = false;
      }
    }while(isShape == false);
    
double dimension1; // dimension variable types defined and named to be later changed inside loops  
double dimension2;   
     if(shape.equals("circle")){ // this commans will run if the shape typed was a circle
      do {
        System.out.print("Enter the radius of the circle: "); // this new do loop will read the dimension values
        // checks if it is not a double
        while (!myScan.hasNextDouble()) { // if input not a double ...
            // gets rid of the input that was not an integer
            String heightNo = myScan.next();  // asks user again for an input
            System.out.printf("Not a valid number, enter again: ");
        }
        // now store the value of the integer to use later in the program
        dimension1 = myScan.nextDouble();
        // end of do while loop
        } while (dimension1 <= 0); // this ends the do loop
       
       double areaCircle = areaCircleMethod(dimension1); // call the area method
       System.out.println("The area of the circle is: " + areaCircle); // print

      }else if(shape.equals("rectangle")){ // same idea but now the shape typed was a rectangle
        do {
          System.out.print("Enter the base of the rectanle: "); // ask user inputs for dimensions
          // checks if it is not a double
          while (!myScan.hasNextDouble()) {
              // gets rid of the input that was not an integer
              String heightNo = myScan.next();
              // asks user again for an input
              System.out.printf("Not a valid number, enter again: ");
          }
          // not stores the value of the integer to use later in the program
          dimension1 = myScan.nextDouble();
          // end of do while loop
          } while (dimension1 <= 0);
       
       do {
          System.out.print("Enter the height of the rectanle: ");
          // checks if it is not a double
          while (!myScan.hasNextDouble()) {
              // gets rid of the input that was not an integer
              String heightNo = myScan.next();
              // asks user again for an input
              System.out.printf("Not a valid number, enter again: ");
          }
          // not stores the value of the integer to use later in the program
          dimension2 = myScan.nextDouble();
          // end of do while loop
          } while (dimension2 <= 0);
       
         double areaRectanle = areaRectangleMethod(dimension1, dimension2);
         System.out.println("The area of the rectangle is: " + areaRectanle);
       
      }else if(shape.equals("triangle")){ // same concept but now shaped types is a triangle

        do {
          System.out.print("Enter the base of the triangle: ");
          // checks if it is not a double
          while (!myScan.hasNextDouble()) {
              // gets rid of the input that was not an integer
              String heightNo = myScan.next();
              // asks user again for an input
              System.out.printf("Not a valid number, enter again: ");
          }
          // not stores the value of the integer to use later in the program
          dimension1 = myScan.nextDouble();
          // end of do while loop
          } while (dimension1 <= 0);
       
       do {
          System.out.print("Enter the height of the triangle: ");
          // checks if it is not a double
          while (!myScan.hasNextDouble()) {
              // gets rid of the input that was not an integer
              String heightNo = myScan.next();
              // asks user again for an input
              System.out.printf("Not a valid number, enter again: ");
          }
          // not stores the value of the integer to use later in the program
          dimension2 = myScan.nextDouble();
          // end of do while loop
          } while (dimension2 <= 0);
       
         double areaTriangle = areaTriangleMethod(dimension1, dimension2);

         System.out.println("The area of the rectangle is: " + areaTriangle);
      }
  }
  
  // Methods called to calculare the area
  public static double areaCircleMethod(double a){ // this method will be called to a circle shape
    double area = a * a * 3.14;
    return area; // return it to main method
  }
  
  public static double areaRectangleMethod(double a, double b){ // they can all have same input names
    double area = a * b;
    return area; // and also area names can be the same
  }
  
  public static double areaTriangleMethod(double a, double b){ 
    double area = a * b / 2;
    return area;
  }
} // close class