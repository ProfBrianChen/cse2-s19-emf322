// CSE 002 Homework 04
// Emma Fernandez 
// Feb 18, 2019


// This program first draws five random cards from five diferent decks
// Then it evaluates the cards and then determines if there is a pair, two pair, three of a kind
// four of a kind or if they are alll the same.
// If there isnt any combination, it will read a high high card hand.


public class PokerHandCheck{
  // required class and main method
  
  public static void main (String args[]){
    
    // draw the random card from FIVE different decks using the math random command
    int randomNumber1 = (int) (Math.random() * 51 + 1);
    int randomNumber2 = (int) (Math.random() * 51 + 1);
    int randomNumber3 = (int) (Math.random() * 51 + 1);
    int randomNumber4 = (int) (Math.random() * 51 + 1);    
    int randomNumber5 = (int) (Math.random() * 51 + 1);
    
    /* print out numbers (to check code)
    System.out.println(randomNumber1);
    System.out.println(randomNumber2);
    System.out.println(randomNumber3);
    System.out.println(randomNumber4);
    System.out.println(randomNumber5);
    */
    
    
// first deck of cards (Just copy paste from the lab)
    //initializing string variables
    String cardNumber1 = "number";
    String suitName1 = "name";
    
    System.out.println("You picked: ");
    // modulud operator for card number
    int cardDivision1 = randomNumber1 % 13;
    // determine the suit
    switch(cardDivision1){
      case 1:
          cardNumber1 = "Ace of ";
          break;
      case 11:
        cardNumber1 = "Jack of ";
        break;
      case 12:
        cardNumber1 = "Queen of ";
        break;
      case 0:
        cardNumber1 =  "King of ";
        break;
      default:
        cardNumber1= cardDivision1 + " of ";        
    }
    
    if(randomNumber1 <= 13){
      suitName1 = "diamonds";
    } 
      else if (randomNumber1 <= 26){
      suitName1 = "clubs";      
    }
    else if (randomNumber1 <= 39){
      suitName1 = "hearts";
    }
    else if (randomNumber1 <= 52){
      suitName1 = "spades";
    }
    System.out.println("    " + cardNumber1 + suitName1);
    
// second deck
    
        //initializing string variable
    String cardNumber2 = "number";
    String suitName2 = "name";
    
    int cardDivision2 = randomNumber2 % 13;
    
    switch(cardDivision2){
      case 1:
          cardNumber2 = "Ace of ";
          break;
      case 11:
        cardNumber2 = "Jack of ";
        break;
      case 12:
        cardNumber2 = "Queen of ";
        break;
      case 0:
        cardNumber2 =  "King of ";
        break;
      default:
        cardNumber2= cardDivision2 + " of ";        
    }
    
    if(randomNumber2 <= 13){
      suitName1 = "diamonds";
    } 
      else if (randomNumber2 <= 26){
      suitName2 = "clubs";      
    }
    else if (randomNumber2 <= 39){
      suitName2 = "hearts";
    }
    else if (randomNumber2 <= 52){
      suitName2 = "spades";
    }
    System.out.println("    " + cardNumber2 + suitName2);
    
//third deck of cards
    
        //initializing string variable
    String cardNumber3 = "number";
    String suitName3 = "name";
    
    int cardDivision3 = randomNumber3 % 13;
    
    switch(cardDivision3){
      case 1:
          cardNumber3 = "Ace of ";
          break;
      case 11:
        cardNumber3 = "Jack of ";
        break;
      case 12:
        cardNumber3 = "Queen of ";
        break;
      case 0:
        cardNumber3 =  "King of ";
        break;
      default:
        cardNumber3= cardDivision3 + " of ";        
    }
    
    if(randomNumber3 <= 13){
      suitName3 = "diamonds";
    } 
      else if (randomNumber3 <= 26){
      suitName3 = "clubs";      
    }
    else if (randomNumber3 <= 39){
      suitName3 = "hearts";
    }
    else if (randomNumber3 <= 52){
      suitName3 = "spades";
    }
    System.out.println("    " + cardNumber3 + suitName3);
    
// fourth deck of cards
        //initializing string variable
    String cardNumber4 = "number";
    String suitName4 = "name";
    
    int cardDivision4 = randomNumber4 % 13;
    
    switch(cardDivision4){
      case 1:
          cardNumber4 = "Ace of ";
          break;
      case 11:
        cardNumber4 = "Jack of ";
        break;
      case 12:
        cardNumber4 = "Queen of ";
        break;
      case 0:
        cardNumber4 =  "King of ";
        break;
      default:
        cardNumber4= cardDivision4 + " of ";        
    }
    
    if(randomNumber4 <= 13){
      suitName4 = "diamonds";
    } 
      else if (randomNumber4 <= 26){
      suitName4 = "clubs";      
    }
    else if (randomNumber4 <= 39){
      suitName4 = "hearts";
    }
    else if (randomNumber4 <= 52){
      suitName4 = "spades";
    }
    System.out.println("    " + cardNumber4 + suitName4);
    
//fifth deck of cardNumber
        //initializing string variable
    String cardNumber5 = "number";
    String suitName5 = "name";
    
    int cardDivision5 = randomNumber5 % 13;
    
    switch(cardDivision5){
      case 1:
          cardNumber5 = "Ace of ";
          break;
      case 11:
        cardNumber5 = "Jack of ";
        break;
      case 12:
        cardNumber5 = "Queen of ";
        break;
      case 0:
        cardNumber5 =  "King of ";
        break;
      default:
        cardNumber5= cardDivision5 + " of ";        
    }
    
    if(randomNumber5 <= 13){
      suitName5 = "diamonds";
    } 
    else if (randomNumber5 <= 26){
      suitName5 = "clubs";      
    }
    else if (randomNumber5 <= 39){
      suitName5 = "hearts";
    }
    else if (randomNumber5 <= 52){
      suitName5 = "spades";
    }
    System.out.println("    " + cardNumber5 + suitName5);

//
// After drawing the five cards, we will identify if there is any combination
//
    /* print out number (0 to 12) to make sure code is right
    System.out.println(cardDivision1);
    System.out.println(cardDivision2);
    System.out.println(cardDivision3);
    System.out.println(cardDivision4);
    System.out.println(cardDivision5);
    */
    
    
    // Initialize variable for count
    int count = 0;
    
    // define booleans to identify if there are any pairs with the first card
    boolean boolean1 = false ; 
    boolean boolean2 = false ; 
    boolean boolean3 = false ; 
    boolean boolean4 = false ;
    boolean boolean5 = false ; 
    
    
    // command for count for first card 
    // if any of these cards are equal, a boolean will be true and the count +1
    if (cardDivision1 == cardDivision2){
      count += 1;
      boolean1 = true; 
    }
    if(cardDivision1 == cardDivision3){
      count += 1; 
      boolean2 = true;   
    }
    if(cardDivision1 == cardDivision4){ 
      count += 1; 
      boolean3 = true; 
    }
    if(cardDivision1 == cardDivision5){ 
      count += 1; 
      boolean4 = true; 
      
    }
   
    // Main switch operator to identify other cards
    switch(count){
      case 4: // if ever time count +=1
      System.out.println("All cards are the same"); 
      break;
    
      case 3:
      System.out.println("Four of a kind");
      break;
    
      case 2: 
      System.out.println("Three of a kind");
      break;
    
      case 1: // when there is one pair with the first card
      // Checking for other pairs
      if (boolean1 && ( (cardDivision3 == cardDivision4) || (cardDivision4 == cardDivision5) || (cardDivision5 == cardDivision3))){
         System.out.println("You have two pairs"); 
      } 
      else if  (boolean2 && ( (cardDivision2 == cardDivision4) || (cardDivision4 == cardDivision5) || (cardDivision5 == cardDivision2))){
         System.out.println("You have two pairs"); 
      }
      else if   (boolean3 && ( cardDivision3 == cardDivision2) || (cardDivision3 == cardDivision5) || (cardDivision5 == cardDivision2)){
         System.out.println("You have two pairs"); 
      }
      else if (boolean4 && ( (cardDivision2 == cardDivision3) || (cardDivision3 == cardDivision4) || (cardDivision2 == cardDivision4))){
         System.out.println("You have two pairs");  
      }
      else { 
      System.out.println("You have one pair ");
      }
      break; 
        
      
    case 0: // If the first card is not equal to any other
    //Checking for combinations in the other four
    if ( (cardDivision2 == cardDivision3) && (cardDivision3 == cardDivision4) && (cardDivision4 == cardDivision5)){ 
        System.out.println(" You have 4 of a kind"); 
    }
    // Checking for three of a kind without the first card
    if (  (cardDivision2 == cardDivision3) && (cardDivision2 == cardDivision4)){
      System.out.println("You have three of a kind");
      break;
    }
     if (  (cardDivision2 == cardDivision3) && (cardDivision2 == cardDivision5)){
      System.out.println("You have three of a kind");
       break;
    }
    if (  (cardDivision2 == cardDivision4) && (cardDivision2 == cardDivision5)){
      System.out.println("You have three of a kind");
      break; 
    }
    if (  (cardDivision3 == cardDivision4) && (cardDivision3 == cardDivision5)){
      System.out.println("You have three of a kind");
      break;
   
    }
    // Checking for pairs within the other four cards
    if  ( (cardDivision2 == cardDivision3) || (cardDivision2== cardDivision4) || (cardDivision2 == cardDivision5)){
         System.out.println("You have one pair"); 
      } 
   else if ( (cardDivision3 == cardDivision4) || (cardDivision3 == cardDivision5 )){
         System.out.println("You have one pair"); 
      }
    else if (cardDivision4 == cardDivision5){
      System.out.println("You have one pair");
      
    }
     else { 
     System.out.println( "You have a high card hand ");
     }
    break;
    }
        

  }
}