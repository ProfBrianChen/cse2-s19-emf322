// CSE 002 Homework 5
// Emma Fernandez
// March 6, 2019

// This program will ask the user for inputs regarding a course they are taking
// asks for the course number, department name, the number of 
 // times it meets in a week, the time the class starts, the instructor name, and the number of students.
    

import java.util.Scanner;
// importing the scanner and the required class and method

public class Hw05 {
  public static void main (String args[]){
    Scanner myScanner = new Scanner(System.in);
     
    
    // First infinite while loop for the coure number
    System.out.println("What is the course number?");
    boolean isCourseNumber = myScanner.hasNextInt();
    
    while (isCourseNumber == false){
      String wrongInputCourse = myScanner.next();
      System.out.println("Enter the course number as an integer: ");
      isCourseNumber = myScanner.hasNextInt();
    }
      
    // Now infinite loop for the department name
    System.out.println("What is the department name?");
   
      boolean isDepartmentNameAnInt = myScanner.hasNextInt();
      boolean isDepartmentNameADouble = myScanner.hasNextDouble();
    
    while (isDepartmentNameAnInt || isDepartmentNameADouble){
      String wrongInputDep = myScanner.next();
      System.out.println("Enter the department name as a string: ");
      isDepartmentNameAnInt = myScanner.hasNextInt();
      isDepartmentNameADouble = myScanner.hasNextDouble();
    }
 
  
    // Number of times they meet per week
        // First infinite while loop for the coure number
    System.out.println("How many times do you meet per week?");
    boolean isPerWeekInt = myScanner.hasNextInt();
    
    while(isPerWeekInt == false ){
      String wrongNumberPerWeek = myScanner.next();
      System.out.println("Enter the number of weekly meetings as an integer: ");
      isPerWeekInt = myScanner.hasNextInt();
    }

    
    // loop for the time of the class
    
    // NOTE:
    // I had a printing error when it got to this step
    // if did not allow an input from the user, but when I placed it at the beginning of the code it did work
    // I tried to check every detail and I dont see where there is an error
    // sorry! ... it works perfectly fine if it is in another section of the code,
    // but I wanted to follow the order of the assignment
    
    System.out.println("At what time is the class? Can answer in the format XX.XX (hour.minutes)");
    boolean isHourDouble = myScanner.hasNextDouble();
    
    while (isHourDouble == false){
      String wrongHour = myScanner.next();
      System.out.println("Error: enter as a double");
      isHourDouble = myScanner.hasNextDouble();
    }
    
    // Loop for professor's name
    System.out.println("What is the professor's name?");
   
      boolean isProfNameInt = myScanner.hasNextInt();
      boolean isProfNameDouble = myScanner.hasNextDouble();
    
    while (isProfNameInt || isProfNameDouble){
      String wrongProfName = myScanner.next();
      System.out.println("Enter the professor's name as a string: ");
      isProfNameInt = myScanner.hasNextInt();
      isProfNameDouble = myScanner.hasNextDouble();
    }
    
    
    // Loop for number of Students
     System.out.println("What is the number of students?");
    boolean isStudNumberInt = myScanner.hasNextInt();
    
    while (isStudNumberInt == false){
      String wrongStudentNumber = myScanner.next();
      System.out.println("Enter the number of students as an integer: ");
      isStudNumberInt = myScanner.hasNextInt();
    }
    
    
  }
}